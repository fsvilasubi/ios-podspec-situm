Pod::Spec.new do |s|  
    s.name              = 'SitumSDKFer'
    s.version           = '2.1.1'
    s.summary           = 'Situm SDK'
    s.static_framework = true
    
    s.homepage         = 'https://github.com/fsvilas/ios-pod-situmsdk'
    s.author            = { 'Name' => 'sdk@example.com' }
    s.license           = { :type => 'Apache-2.0', :file => 'LICENSE' }

    s.platform          = :ios
    s.source            = { :http => 'https://github.com/fsvilas/ios-pod-situmsdk/raw/master/SitumSDK.zip' }

    s.ios.deployment_target = '9.0'
    s.ios.vendored_frameworks = 'SitumSDK.framework'
    s.frameworks = 'CoreLocation', 'CoreMotion'
    s.libraries = 'c++','z'
    s.pod_target_xcconfig = {
    'OTHER_LDFLAGS' => '$(inherited) -ObjC'
    }


end  
