Pod::Spec.new do |s|
 s.name = 'SitumSDK'
 s.version = '0.1.0'
 s.summary = 'Indoor Location for iOS.'
 s.static_framework = true

 s.homepage = 'http://developers.situm.es/pages/mobile/ios/'
 s.author = { 'Name' => 'support@situm.es' }
 s.license = { :type => 'CUSTOM', :file => 'LICENSE.txt' }
 s.platform = :ios
 s.source = { :git => 'https://adrianRodriguezVilas@bitbucket.org/situm/ios-library-situmsdk.git', :branch => 'feature/i18n', :tag => 'test0.1.0'}
 s.ios.deployment_target = '8.0'
 s.ios.vendored_frameworks = 'SitumSDK.framework'
 s.frameworks = 'CoreLocation', 'CoreMotion'
 s.libraries = 'c++','z'
 s.pod_target_xcconfig = {
 'OTHER_LDFLAGS' => '$(inherited) -ObjC'
 }
end
