Pod::Spec.new do |s|
 s.name = 'SitumSDK'
 s.version = '2.31.2'
 s.summary = 'Indoor Location for iOS.'
 s.static_framework = true

 s.homepage = 'http://developers.situm.es/pages/mobile/ios/'
 s.author = { 'Name' => 'support@situm.es' }
 s.license = { :type => 'CUSTOM', :file => 'LICENSE.txt' }
 s.platform = :ios
 s.source = { :http => 'https://ci.situm.es/artifactory/libs-snapshot-local/iOS/SitumSDK/feature-remove-static-deps/2.31.2/SitumSDK.noprotobuf-feature-remove-static-deps-2.31.2-DEV.zip' }
 s.ios.deployment_target = '9.0'
 s.ios.vendored_frameworks = 'SitumSDK.framework'
 s.frameworks = 'CoreLocation', 'CoreMotion'
 s.libraries = 'c++','z'
 s.dependency 'Protobuf', '~> 3.7'
 s.pod_target_xcconfig = {
 'OTHER_LDFLAGS' => '$(inherited) -ObjC'
 }
end
