Pod::Spec.new do |s|
 s.name = 'SitumSDK'
 s.version = '3.10.0'
 s.summary = 'Indoor Location for iOS.'
 s.static_framework = true

 s.homepage = 'http://developers.situm.com/pages/mobile/ios/'
 s.author = { 'Name' => 'support@situm.com' }
 s.license = { :type => 'CUSTOM', :file => 'LICENSE.txt' }
 s.platform = :ios
 s.source = { :http => 'https://repo.situm.com/artifactory/libs-release-local/iOS/SitumSDK/3.10.0/SitumSDK.xcframework.noprotobuf.zip' }
 s.ios.deployment_target = '9.0'
 s.ios.vendored_frameworks = 'SitumSDK.xcframework'
 s.frameworks = 'CoreLocation', 'CoreMotion'
 s.libraries = 'c++','z'
 s.dependency 'Protobuf', '~> 3.7'
 s.dependency 'SSZipArchive', '~> 2.4'
 s.pod_target_xcconfig = {
 'OTHER_LDFLAGS' => '$(inherited) -ObjC'
 }
end
