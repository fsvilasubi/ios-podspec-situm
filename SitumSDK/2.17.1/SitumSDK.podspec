Pod::Spec.new do |s|
 s.name = 'SitumSDK'
 s.version = '2.17.1'
 s.summary = 'Indoor Location for iOS.'
 s.static_framework = true

 s.homepage = 'http://developers.situm.es/pages/ios/'
 s.author = { 'Name' => 'support@situm.es' }
 s.license = { :type => 'CUSTOM', :file => 'LICENSE.txt' }
 s.platform = :ios
 s.source = { :http => 'https://repo.situm.es/artifactory/libs-release-local/iOS/SitumSDK/2.17.1/SitumSDK.zip' }
 s.ios.deployment_target = '8.0'
 s.ios.vendored_frameworks = 'SitumSDK.framework'
 s.frameworks = 'CoreLocation', 'CoreMotion'
 s.libraries = 'c++','z'
 s.pod_target_xcconfig = {
 'OTHER_LDFLAGS' => '$(inherited) -ObjC'
 }
end
