Pod::Spec.new do |s|  
    s.name              = 'SitumSDK'
    s.version           = '2.16.4'
    s.summary           = 'Situm SDK'
    s.static_framework = true
    
    s.homepage         = 'http://developers.situm.es/pages/ios/'
    s.author            = { 'Name' => 'support@situm.es' }
    s.license           = { :type => 'CUSTOM', :file => 'License.txt' }

    s.platform          = :ios
    s.source            = { :http => 'http://repo.situm.es/artifactory/libs-release-local/iOS/SitumSDK/2.16.4/SitumSDK.zip' }

    s.ios.deployment_target = '9.0'
    s.ios.vendored_frameworks = 'SitumSDK.framework'
    s.frameworks = 'CoreLocation', 'CoreMotion'
    s.libraries = 'c++','z'
    s.pod_target_xcconfig = {
    'OTHER_LDFLAGS' => '$(inherited) -ObjC'
    }
end  
