Pod::Spec.new do |s|
 s.name = 'SitumWayfindingPrivate'
 s.version = '0.1.9'
 s.summary = 'Indoor Location for iOS.'

 s.homepage = 'http://developers.situm.es/pages/mobile/ios/'
 s.author = { 'Name' => 'support@situm.es' }
 s.license = { :type => 'CUSTOM', :file => 'LICENSE.txt' }
 s.platform = :ios
 s.source = { :http => 'https://github.com/fsvilas/wayfinding-fer/blob/master/SitumWayfinding.zip?raw=true' }
 s.ios.deployment_target = '9.0'
 s.ios.vendored_frameworks = 'SitumWayfinding.framework'
 s.dependency 'GoogleMaps', '~>3.1.0'
 s.dependency 'SitumSDK'
end
