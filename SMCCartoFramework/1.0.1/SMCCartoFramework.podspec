Pod::Spec.new do |s|  
    s.name              = 'SMCCartoFramework'
    s.version           = '1.0.1'
    s.summary           = 'Map module for the SITUM sdk'
    s.static_framework = true
    
    s.homepage         = 'https://github.com/fsvilas/ios-pod-smccartoframework'
    s.author            = { 'Name' => 'sdk@example.com' }
    s.license           = { :type => 'Apache-2.0', :file => 'LICENSE' }

    s.platform          = :ios
    s.source            = { :http => 'https://github.com/fsvilas/ios-pod-smccartoframework/raw/master/SMCCartoFramework.zip' }

    s.ios.deployment_target = '9.0'
    s.ios.vendored_frameworks = 'SMCCartoFramework.framework'
    s.resources = 'SMCCartoFramework.framework/Carto.bundle'
    s.dependency 'GoogleMaps'
    s.dependency 'SitumSDKFer'
end 