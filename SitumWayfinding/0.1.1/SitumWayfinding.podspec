Pod::Spec.new do |s|
 s.name = 'SitumWayfinding'
 s.version = '0.1.1'
 s.summary = 'Indoor Location for iOS.'
 s.static_framework = true

 s.homepage = 'http://developers.situm.es/pages/mobile/ios/'
 s.author = { 'Name' => 'support@situm.es' }
 s.license = { :type => 'CUSTOM', :file => 'LICENSE.txt' }
 s.platform = :ios
 s.source = { :http => 'https://ci.situm.es/artifactory/libs-release-local/iOS/SitumWayfinding/master/0.1.1/SitumWayfinding-0.1.1.zip' }
 s.ios.deployment_target = '9.0'
 s.ios.vendored_frameworks = 'SitumWayfinding.framework'
 s.dependency 'GoogleMaps', '~> 2.7.0'
 s.dependency 'SitumSDK', '~> 2.31.1'
end
