#
# Be sure to run `pod lib lint SitumWayfindingPod.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'SitumWayfindingPod'
  s.version          = '0.1.7'
  s.summary          = 'SitumWayfinding Pod'
  s.static_framework = true

  s.description      = <<-DESC
    With Situm IPS platform you can develop your wayfinding solution from zero, and with the module Situm WYF you can easily integrate guiding functionality in an existing APP to improve your visitors experience, whether in hospitals, malls, airports, corporate headquarters or convention centers.
                       DESC

  s.homepage         = 'https://github.com/fsvilas/SitumWayfindingPod'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'fsvilas' => 'fsvilas@situm.com' }
  s.source           = { :git => 'https://fsanchezvilas@bitbucket.org/fsanchezvilas/ios-pod-wayfinding.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'
  s.swift_version = '5.0'

  s.source_files = 'SitumWayfindingPod/Classes/**/*'
  
  s.resources = ['SitumWayfindingPod/Assets/*.storyboard', 'SitumWayfindingPod/Assets/Images/**/*.png']
  
  #s.resource_bundles = {
  #  'SitumWayfindingPod' => ['SitumWayfindingPod/Assets/*.{storyboard,xcassets}']
  #}

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
   s.dependency 'GoogleMaps', '~> 3.1.0'
   s.dependency 'SitumSDK'
end
